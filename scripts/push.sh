#!/bin/sh
set -e

REVISION=$1

source ./scripts/env.sh

docker push registry.gitlab.com/ssquared/ssquared-public/react-static-nginx:${NGINX_VERSION}-${REVISION}